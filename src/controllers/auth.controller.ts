import { Request, Response } from 'express'
import { User } from '../entity/User'

// -------- Agregar para jwt
import jwt from 'jsonwebtoken'
import { comparePassword, createHash } from '../helpers/bcryptHelper'

const jwtSecret = 'somesecrettoken'
const jwtRefreshTokenSecret = 'somesecrettokenrefresh'
let refreshTokens: (string | undefined)[] = []

const createToken = (user: User) => {
  // Se crean el jwt y refresh token
  const token = jwt.sign({ id: user.id, username: user.username }, jwtSecret, {
    expiresIn: '120m',
  })
  const refreshToken = jwt.sign(
    { username: user.username },
    jwtRefreshTokenSecret,
    { expiresIn: '90d' }
  )

  refreshTokens.push(refreshToken)
  return {
    token,
    refreshToken,
  }
}
// ----------

export const signUp = async (
  req: Request,
  res: Response
): Promise<Response> => {
  if (!req.body.username || !req.body.password) {
    return res
      .status(400)
      .json({ msg: 'Please. Send your username and password' })
  }

  const user = await User.findOneBy({ username: req.body.username })
  if (user) {
    return res.status(400).json({ msg: 'The User already Exists' })
  }

  const newUser = new User()
  newUser.username = req.body.username
  newUser.password = await createHash(req.body.password)
  await newUser.save()
  return res.status(201).json(newUser)
}

export const signIn = async (
  req: Request,
  res: Response
): Promise<Response> => {
  if (!req.body.username || !req.body.password) {
    return res
      .status(400)
      .json({ msg: 'Please. Send your username and password' })
  }

  const user = await User.findOneBy({ username: req.body.username })
  if (!user) {
    return res.status(400).json({ msg: 'The User does not exists' })
  }

  const isMatch = await comparePassword(user, req.body.password)
  if (isMatch) {
    return res.status(400).json({ credentials: createToken(user) })
  }

  return res.status(400).json({
    msg: 'The username or password are incorrect',
  })
}

export const protectedEndpoint = async (
  req: Request,
  res: Response
): Promise<Response> => {
  return res.status(200).json({ msg: 'ok' })
}

// Create new access token from refresh token
export const refresh = async (req: Request, res: Response): Promise<any> => {
  // const refreshToken = req.header("x-auth-token");

  const refreshToken = req.body.refresh

  // If token is not provided, send error message
  if (!refreshToken) {
    res.status(401).json({
      errors: [
        {
          msg: 'Token not found',
        },
      ],
    })
  }

  console.log(refreshTokens)
  // If token does not exist, send error message
  if (!refreshTokens.includes(refreshToken)) {
    res.status(403).json({
      errors: [
        {
          msg: 'Invalid refresh token',
        },
      ],
    })
  }

  try {
    const user = jwt.verify(refreshToken, jwtRefreshTokenSecret)
    // user = { email: 'jame@gmail.com', iat: 1633586290, exp: 1633586350 }
    const { username } = <any>user

    const userFound = <User>await User.findOneBy({ username: username })
    if (!user) {
      return res.status(400).json({ msg: 'The User does not exists' })
    }

    const accessToken = jwt.sign(
      { id: userFound.id, username: userFound.username },
      jwtSecret,
      { expiresIn: '120s' }
    )

    res.json({ accessToken })
  } catch (error) {
    res.status(403).json({
      errors: [
        {
          msg: 'Invalid token',
        },
      ],
    })
  }
}
