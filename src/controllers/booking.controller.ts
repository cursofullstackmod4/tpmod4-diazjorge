import { Request, Response } from 'express'

import { Booking } from './../entity/Booking'
import { User } from '../entity/User'
import { Event } from '../entity/Event'

export const getBookings = async (req: Request, res: Response) => {
  try {
    const bookings = await Booking.find({
      relations: {
        event: true,
        user: true,
      },
    })
    return res.json(bookings)
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message })
    }
  }
}

export const getBooking = async (req: Request, res: Response) => {
  try {
    const { id } = req.params
    const booking = await Booking.findOne({
      where: { id: parseInt(id) },
      relations: ['user', 'event'],
    })

    if (!booking) return res.status(404).json({ message: 'Booking not found' })

    return res.json(booking)
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message })
    }
  }
}

export const createBooking = async (req: Request, res: Response) => {
  try {
    const { userid, eventid } = req.body

    const user = await User.findOne({
      where: { id: parseInt(userid) },
    })
    const event = await Event.findOne({
      where: { id: parseInt(eventid) },
    })
    if (!user) return res.status(404).json({ message: 'User not found' })
    if (!event) return res.status(404).json({ message: 'Event not found' })
    if (event.cupoDisponible === 0)
      return res
        .status(404)
        .json({ message: 'No hay mas cupo disponible para este evento' })

    const booking = new Booking()
    booking.precio = event.precio
    booking.fechaHora = event.fechaHora
    booking.lugar = event.lugar
    booking.gps = event.gps
    booking.user = user
    booking.event = event

    await booking.save()
    let cupo = event.cupoDisponible
    event.cupoDisponible = cupo - 1
    await event.save()

    return res.json(booking)
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message })
    }
  }
}

export const updateBooking = async (req: Request, res: Response) => {
  const { id } = req.params

  try {
    const booking = await Booking.findOneBy({ id: parseInt(id) })
    if (!booking) return res.status(404).json({ message: 'Not booking found' })

    await Booking.update({ id: parseInt(id) }, req.body)

    return res.sendStatus(204)
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message })
    }
  }
}

export const deleteBooking = async (req: Request, res: Response) => {
  const { id } = req.params
  try {
    const result = await Booking.delete({ id: parseInt(id) })

    if (result.affected === 0)
      return res.status(404).json({ message: 'Booking not found' })

    return res.sendStatus(204)
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message })
    }
  }
}
