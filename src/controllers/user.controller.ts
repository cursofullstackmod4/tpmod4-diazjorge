import { Request, Response } from 'express'
import { User } from '../entity/User'

// -------- Agregar para jwt
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import { createHash } from '../helpers/bcryptHelper'
const jwtSecret = 'somesecrettoken'
const jwtRefreshTokenSecret = 'somesecrettokenrefresh'
let refreshTokens: (string | undefined)[] = []

const createToken = (user: User) => {
  // Se crean el jwt y refresh token
  const token = jwt.sign({ id: user.id, username: user.username }, jwtSecret, {
    expiresIn: '120m',
  })
  const refreshToken = jwt.sign(
    { username: user.username },
    jwtRefreshTokenSecret,
    { expiresIn: '90d' }
  )

  refreshTokens.push(refreshToken)
  return {
    token,
    refreshToken,
  }
}
// ----------

export const getUsers = async (req: Request, res: Response) => {
  try {
    const users = await User.find({
      select: {
        id: true,
        username: true,
        createdAt: true,
        updatedAt: true,
      },
    })
    return res.json(users)
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message })
    }
  }
}

export const getUser = async (req: Request, res: Response) => {
  try {
    const { id } = req.params
    const user = await User.findOne({
      where: { id: parseInt(id) },
      select: { id: true, username: true, createdAt: true, updatedAt: true },
    })

    if (!user) return res.status(404).json({ message: 'User not found' })

    return res.json(user)
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message })
    }
  }
}

export const createUser = async (req: Request, res: Response) => {
  const { username, password } = req.body

  const user = new User()
  user.username = username
  user.password = password
  await user.save()

  return res.json(user)
}

export const updateUser = async (req: Request, res: Response) => {
  const { id } = req.params

  try {
    const user = await User.findOneBy({ id: parseInt(id) })
    if (!user) return res.status(404).json({ message: 'Not user found' })
    if(req.body.username) user.username = req.body.username
    if(req.body.password) user.password = await createHash(req.body.password)
    await user.save()

    return res.sendStatus(204)
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message })
    }
  }
}

export const deleteUser = async (req: Request, res: Response) => {
  const { id } = req.params
  try {
    const result = await User.delete({ id: parseInt(id) })

    if (result.affected === 0)
      return res.status(404).json({ message: 'User not found' })

    return res.sendStatus(204)
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message })
    }
  }
}
