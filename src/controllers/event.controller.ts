import { Request, Response } from 'express'
import { Event } from '../entity/Event'

interface EventBody {
  type: string
  name: string
}

export const getEvents = async (req: Request, res: Response) => {
  try {
    const events = await Event.find()
    return res.json(events)
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message })
    }
  }
}

export const getEvent = async (req: Request, res: Response) => {
  try {
    const { id } = req.params
    const event = await Event.findOneBy({ id: parseInt(id) })

    if (!event) return res.status(404).json({ message: 'Event not found' })

    return res.json(event)
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message })
    }
  }
}

export const createEvent = async (req: Request, res: Response) => {
  const {
    nombre,
    descripcion,
    lugar,
    fechaHora,
    gps,
    precio,
    limite,
    tipoEvento,
  } = req.body
  try {
    const event = new Event()
    event.nombre = nombre
    event.descripcion = descripcion
    event.lugar = lugar
    event.fechaHora = fechaHora
    event.gps = gps
    event.precio = precio
    event.limite = limite
    event.cupoDisponible = limite
    event.tipoEvento = tipoEvento
    await event.save()
    return res.json(event)
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message })
    }
  }
}

export const updateEvent = async (req: Request, res: Response) => {
  const { id } = req.params

  try {
    const event = await Event.findOneBy({ id: parseInt(id) })
    if (!event) return res.status(404).json({ message: 'Not Event found' })

    await Event.update({ id: parseInt(id) }, req.body)

    return res.sendStatus(204)
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message })
    }
  }
}

export const deleteEvent = async (req: Request, res: Response) => {
  const { id } = req.params
  try {
    const result = await Event.delete({ id: parseInt(id) })

    if (result.affected === 0)
      return res.status(404).json({ message: 'Event not found' })

    return res.sendStatus(204)
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message })
    }
  }
}
