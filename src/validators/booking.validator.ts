import { NextFunction, Request, Response } from 'express'
import { check } from 'express-validator'
import { validateResult } from '../helpers/validateHelper'

export const validateBookingCreate = [
  check('userid')
    .exists()
    .notEmpty()
    .withMessage('Este campo es requerido')
    .isInt()
    .withMessage('Este campo debe ser entero'),
  check('eventid')
    .exists()
    .notEmpty()
    .withMessage('Este campo es requerido')
    .isInt()
    .withMessage('Este campo debe ser entero'),
  (req: Request, res: Response, next: NextFunction) => {
    validateResult(req, res, next)
  },
]

export const validateBookingUpdate = [
    check('userid')
      .isInt()
      .withMessage('Este campo debe ser entero'),
    check('eventid')
      .isInt()
      .withMessage('Este campo debe ser entero'),
    (req: Request, res: Response, next: NextFunction) => {
      validateResult(req, res, next)
    },
  ]
