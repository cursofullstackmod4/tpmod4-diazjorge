import { Request, Response, NextFunction } from 'express'
import { check } from 'express-validator'
import { validateResult } from '../helpers/validateHelper'

export const validateCreateUser = [
  check('username')
    .exists()
    .notEmpty()
    .withMessage('Este campo es requerido')
    .isLength({ min: 4 }),
  check('password')
    .exists()
    .notEmpty()
    .withMessage('Este campo es requerido')
    .isLength({ min: 6 }),
  (req: Request, res: Response, next: NextFunction) => {
    validateResult(req, res, next)
  },
]
export const validateUpdateUser = [
  check('username')
    .isLength({ min: 4 })
    .withMessage('La longitud debe ser mayor a 4'),
  check('password')
    .isLength({ min: 6 })
    .withMessage('La longitud debe ser mayor a 6'),
  (req: Request, res: Response, next: NextFunction) => {
    validateResult(req, res, next)
  },
]