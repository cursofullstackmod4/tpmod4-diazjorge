import { Request, Response, NextFunction } from 'express'
import { check } from 'express-validator'
import { validateResult } from '../helpers/validateHelper'

export const validateCreateEvent = [
  check('nombre').exists().notEmpty().withMessage('Este campo es requerido'),
  check('descripcion')
    .exists()
    .notEmpty()
    .withMessage('Este campo es requerido'),
  check('lugar').exists().notEmpty().withMessage('Este campo es requerido'),
  check('gps').exists().notEmpty().withMessage('Este campo es requerido'),
  check('precio')
    .exists()
    .notEmpty()
    .withMessage('Este campo es requerido')
    .isFloat()
    .withMessage('Este campo debe ser un numero real'),
  check('limite')
    .exists()
    .notEmpty()
    .withMessage('Este campo es requerido')
    .isInt()
    .withMessage('Este campo debe ser numerico'),
  check('tipoEvento')
    .exists()
    .notEmpty()
    .withMessage('Este campo es requerido')
    .isIn(['cine', 'teatro', 'recital', 'charla'])
    .withMessage(
      'Solo se permite como tipo de evento cine, teatro, recital o charla'
    ),
  (req: Request, res: Response, next: NextFunction) => {
    validateResult(req, res, next)
  },
]

export const validateUpdateEvent = [
  check('precio').isFloat().withMessage('Este campo debe ser un numero real'),
  check('limite').isInt().withMessage('Este campo debe ser numerico'),
  check('tipoEvento')
    .isIn(['cine', 'teatro', 'recital', 'charla'])
    .withMessage(
      'Solo se permite como tipo de evento cine, teatro, recital o charla'
    ),
  (req: Request, res: Response, next: NextFunction) => {
    validateResult(req, res, next)
  },
]
