import { Router } from 'express'
import {
  createBooking,
  deleteBooking,
  getBooking,
  getBookings,
  updateBooking,
} from '../controllers/booking.controller'
import passport from 'passport'
import {
  validateBookingCreate,
  validateBookingUpdate,
} from '../validators/booking.validator'

const router = Router()

router.get(
  '/bookings',
  passport.authenticate('jwt', { session: false }),
  getBookings
)
router.get(
  '/bookings/:id',
  passport.authenticate('jwt', { session: false }),
  getBooking
)
router.post(
  '/bookings',
  validateBookingCreate,
  passport.authenticate('jwt', { session: false }),
  createBooking
)
router.put(
  '/bookings/:id',
  validateBookingUpdate,
  passport.authenticate('jwt', { session: false }),
  updateBooking
)
router.delete(
  '/bookings/:id',
  passport.authenticate('jwt', { session: false }),
  deleteBooking
)

export default router
