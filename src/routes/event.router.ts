import { Router } from 'express'
import {
  getEvents,
  getEvent,
  createEvent,
  updateEvent,
  deleteEvent,
} from '../controllers/event.controller'
import passport from 'passport'
import {
  validateCreateEvent,
  validateUpdateEvent,
} from '../validators/event.validator'
const router = Router()

router.get('/events', getEvents)
router.get('/events/:id', getEvent)
router.post(
  '/events',
  validateCreateEvent,
  passport.authenticate('jwt', { session: false }),
  createEvent
)
router.put(
  '/events/:id',
  validateUpdateEvent,
  passport.authenticate('jwt', { session: false }),
  updateEvent
)
router.delete(
  '/events/:id',
  passport.authenticate('jwt', { session: false }),
  deleteEvent
)

export default router
