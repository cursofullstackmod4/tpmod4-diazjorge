import { Router } from 'express'
import {
  getUsers,
  getUser,
  createUser,
  updateUser,
  deleteUser,
} from '../controllers/user.controller'
import passport from 'passport'
import { validateUpdateUser } from '../validators/user.validator'

const router = Router()

router.get('/users', getUsers)

router.get('/users/:id', getUser)
//router.post("/users", passport.authenticate('jwt', { session: false }), createUser);
router.put(
  '/users/:id',
  validateUpdateUser,
  passport.authenticate('jwt', { session: false }),
  updateUser
)
router.delete(
  '/users/:id',
  passport.authenticate('jwt', { session: false }),
  deleteUser
)

export default router
