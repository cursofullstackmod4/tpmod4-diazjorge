import { Router } from 'express'
import {
  signUp,
  signIn,
  refresh,
  protectedEndpoint,
} from '../controllers/auth.controller'
import passport from 'passport'
import { validateCreateUser } from '../validators/user.validator'

const router = Router()

//Agregar para jwt
router.post('/signup', validateCreateUser, signUp)
router.post('/signin', signIn)
router.post('/token', refresh)
router.post(
  '/protected',
  passport.authenticate('jwt', { session: false }),
  protectedEndpoint
)

export default router