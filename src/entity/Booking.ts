import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
} from 'typeorm'
import { User } from './User'
import { Event } from './Event'

@Entity()
export class Booking extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number

  @Column()
  precio: number

  @Column({ type: 'datetime', default: () => 'CURRENT_TIMESTAMP' })
  fechaHora: Date

  @Column()
  lugar: string

  @Column({ default: '6,66775' })
  gps: string

  @CreateDateColumn()
  createdAt: Date

  @UpdateDateColumn()
  updatedAt: Date

  @ManyToOne(() => User)
  user: User

  @ManyToOne(() => Event)
  event: Event
}
